/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{

}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Call the function to create grass in the scene
	createGrassMesh();

	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue::White);
 
	// Position the camera
	mCamera->setPosition(150, 50, 150);

	// Look at the position with the camera
	mCamera->lookAt(0, 0, 0);
 
	// Create an instance of a computer graphic modle robot entity
	Ogre::Entity* robot = mSceneMgr->createEntity("robot", "robot.mesh");

	// Create a node and add the robot entity directly to it in the scene
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(robot);
 
	// Create a plane object
	Ogre::Plane plane;

	// Set the plane to Y axis, making it flat
	plane.normal = Ogre::Vector3::UNIT_Y;

	// Set the plane at the point of origin at 0, 0, 0
	plane.d = 0;
 
	// Create a mesh using the plane
	Ogre::MeshManager::getSingleton().createPlane("floor", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, 450.0f, 450.0f, 10, 10, true, 1, 50.0f, 50.0f, Ogre::Vector3::UNIT_Z);

	// Create an instance of a computer graphic model using the mesh
	Ogre::Entity* planeEnt = mSceneMgr->createEntity("plane", "floor");

	// Set the material for the model
	planeEnt->setMaterialName("Examples/GrassFloor");

	// Don't allow the entity to cast shadows on its surrounding environment
	planeEnt->setCastShadows(false);

	// Create a node and add the entity directly to it 
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(planeEnt);
 
	// Create an instance of the grass mesh entity
	Ogre::Entity* grass = mSceneMgr->createEntity("grass", "GrassBladesMesh");

	// Create a static geometry instance
	Ogre::StaticGeometry* sg = mSceneMgr->createStaticGeometry("GrassArea");
 
	// Size of the static geometry
	const int size = 375;

	// Number of grass regions in the static geometry
	const int amount = 20;
 
	// Set the dimensions of the static geometry
	sg->setRegionDimensions(Ogre::Vector3(size, size, size));
 
	// Set the origin of the static geometry
	sg->setOrigin(Ogre::Vector3(-size/2, 0, -size/2));
 
	// Loop throughout the region
	for(int x = -size / 2; x < size /2; x += (size/amount))
	{
		for(int z = -size / 2; z < size / 2; z += (size/amount))
		{
			// Offset to use for random vector calculation
			Ogre::Real r = size / float(amount / 2);
 
			// Calculate random vector for grass position
			Ogre::Vector3 pos(x + Ogre::Math::RangeRandom(-r, r), 0, z + Ogre::Math::RangeRandom(-r, r));
 
			// Calculate random vector for grass scale
			Ogre::Vector3 scale(1, Ogre::Math::RangeRandom(0.9f, 1.1f), 1);
 
			// Declare quaternion for grass orientation
			Ogre::Quaternion orientation;
  
			// Randomly rotate the grass orientation
			orientation.FromAngleAxis(Ogre::Degree(Ogre::Math::RangeRandom(0, 359)), Ogre::Vector3::UNIT_Y);
  
			// Add the entity to the static geometry
			sg->addEntity(grass, pos, orientation, scale);
		}
 
		// Construct the static geometry after the entity was added
		sg->build();
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::createFrameListener();
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene(void)
{
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);

	return ret;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyPressed(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyReleased(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mouseMoved(me);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mousePressed(me, id);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mouseReleased(me, id);

	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::createGrassMesh(void)
{
	// Width of the grass object
	const float width = 25.0f;

	// Height of the grass object
	const float height = 30.0f;
 
	// Declare the manual object
	Ogre::ManualObject grass("GrassObject");
 
	// Declare a vector to define the four corners of one side of the grass mesh
	Ogre::Vector3 vec(width / 2, 0, 0);

	// Declare a quaternion for grass mesh texture rotation
	Ogre::Quaternion rot;

	// Rotate the grass mesh texture by 60 degrees
	rot.FromAngleAxis(Ogre::Degree(60), Ogre::Vector3::UNIT_Y);
 
	// Define the manual object
	grass.begin("Examples/GrassBlades", Ogre::RenderOperation::OT_TRIANGLE_LIST);
 
	// Define the 4 vertices of the quad and set to the texture coordinates
	for(int i = 0; i < 3; ++i)
	{
		// First grass mesh texture vertices
		grass.position(-vec.x, height, -vec.z);

		// First grass mesh texture coordinates
		grass.textureCoord(0, 0);
 
		// Second grass mesh texture vertices
		grass.position(vec.x, height, vec.z);

		// Second grass mesh texture coordinates
		grass.textureCoord(1, 0);
 
		// Third grass mesh texture vertices
		grass.position(-vec.x, 0, -vec.z);

		// Third grass mesh texture coordinates
		grass.textureCoord(0, 1);
 
		// Last grass mesh texture vertices
		grass.position(vec.x, 0, vec.z);

		// Last grass mesh texture coordinates
		grass.textureCoord(1, 1);
 
		// Calculate grass mess texture corner number
		int offset = i * 4;
 
		// Create first triangle that makes grass mesh
		grass.triangle(offset, offset + 3, offset + 1);
 
		// Create second triangle that makes grass mesh
		grass.triangle(offset, offset + 2, offset + 3);
 
		// Rotate the next grass mesh texture vertices
		vec = rot * vec;
	}

	// Finalize the manual object
	grass.end();
 
	// Create mesh out of this object
	grass.convertToMesh("GrassBladesMesh");
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------